package handlers

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/threetopia/gin-skeleton/middlewares"
	"net/http"
)

type Handler struct {
}

func NewHandler(g *gin.Engine, m *middlewares.Middleware) *Handler {
	h := &Handler{}
	g.GET("/", h.Index)
	return h
}

func (h *Handler) Index(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{
		"hello": "hello",
	})
}
