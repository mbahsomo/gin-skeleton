package main

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/threetopia/gin-skeleton/handlers"
	"gitlab.com/threetopia/gin-skeleton/middlewares"
)

func main() {
	g := gin.Default()
	g.Use(gin.Logger())
	m := middlewares.NewMiddleware(g)
	handlers.NewHandler(g, m)
	g.Run() // listen and serve on 0.0.0.0:8080 (for windows "localhost:8080")
}
